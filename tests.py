#!/usr/bin/env python3

import os.path
import sys

class UnitTests:
    def __init__(self, failFast=False):
        self.isFailed = False
        self.failFast = failFast
        self.errors = []

    def logError(self, message):
        self.errors.append(message)

    def makeSuccess(self):
        print('.', end='')

    def makeFail(self):
        print('F', end='')
        self.isFailed = True

    def fileContains(self, filePath, textToFind, errorMessage=''):
        if not self.fileExists(filePath, True):
            return False

        with open(filePath, 'r') as file:
            content = file.read()

            if textToFind in content:
                self.makeSuccess()
            else:
                self.logError(f'{textToFind} could not be found in {filePath}')
                self.makeFail()

    def fileExists(self, filePath, skipDot=False):
        if not os.path.exists(filePath):
            self.logError(f'Missing file: {filePath}')
            self.makeFail()
            return False
        if not skipDot:
            self.makeSuccess()
        return True

    def fileNotEmpty(self, filePath, errorMessage=''):
        if not self.fileExists(filePath, True):
            return False

        with open(filePath, 'r') as file:
            content = file.read()

            if len(content) > 0:
                self.makeSuccess()
            else:
                self.logError(f'File {filePath} is empty')
                self.makeFail()

    def run(self):
        if self.isFailed:
            print(' ')
            print("\n".join(self.errors))
            return sys.exit(1)
        print(' ')
        return sys.exit(0)

if __name__ == '__main__':
    ut = UnitTests(failFast=True)
    ut.fileContains("public/js/external-latest.js", "externalFunction")
    ut.fileExists("public/js/app-latest.js")
#     ut.fileExists("public/fonts/glyphicons-halflings-regular.woff")
#     ut.fileExists("public/fonts/glyphicons-halflings-regular.ttf")
#     ut.fileExists("public/fonts/glyphicons-halflings-regular.svg")
    ut.fileContains("public/css/external-latest.css", "getbootstrap.com")
    ut.fileContains("public/css/stylus-latest.css", "body .stylus")
    ut.fileContains("public/css/scss-latest.css", "body .scss")
    ut.fileContains("public/css/app-latest.css", "body .project")
    ut.run()

