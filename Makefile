.PHONY: test build run gulp

test:
	npm install --no-interaction
	npm install -g gulp
	cp gulp-config.test.js gulp-config.js
	npm install bootstrap gulp-sass sass kisphp-format-string bootstrap @fortawesome/fontawesome-free
	gulp --gulpfile app/gulp/gulpfile.js --cwd .
	echo $?
	tree public
	python3 tests.py
	rm gulp-config.js

build:
	docker build -f docker/Dockerfile -t kisphp-assets .

run:
	docker run --rm -it kisphp-assets bash -c "make test"

gulp:
	rm -rf web/
	gulp --gulpfile app/gulp/gulpfile.js --cwd .
