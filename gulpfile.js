const { task, series } = require('gulp');

const config = require('../../gulp-config');

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}

// external javascript files, will only concatenate files
const js = require('kisphp-assets/tasks/javascripts')(config.js.external);
// local js scripts compiled by browserify
const bsrf = require('kisphp-assets/tasks/browserify')(config.js.project);
// external css file, will only concatenate files
const css = require('kisphp-assets/tasks/css')(config.css.external);
// compile local css file
const incss = requireUncached('kisphp-assets/tasks/css')(config.css.project);
// copy images, fonts to public directory
const files = require('kisphp-assets/tasks/copy_files')(config.files);

task('default', series(
    files.copy_files,
    css.css,
    incss.css,
    js.javascripts,
    bsrf.browserify,
));
