const { task, series } = require('gulp');

/**
 * If you add custom gulp tasks, you'll have to use the next line instead of the first one
 */
// const { task, series, src, dest } = require('gulp');

// add gulp config file
const config = require('../../gulp-config.test');

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}

const js = require('../../tasks/javascripts')(config.js.external);
const bsrf = require('../../tasks/browserify')(config.js.project);
let css = requireUncached('../../tasks/css')(config.css.external);
let incss = requireUncached('../../tasks/css')(config.css.project);
const scss = require('../../tasks/scss')(config.css.scss);
const stl = require('../../tasks/stylus')(config.css.stylus);
const files = require('../../tasks/copy_files')(config.files);

task('default', series(
  bsrf.browserify,
  files.copy_files,
  css.css,
  incss.css,
  js.javascripts,
  scss.scss,
  stl.stylus
));
