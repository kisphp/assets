/**
 *
 * This file is for testing purposes
 *
 */

const settings = function(){
  this.root_path = __dirname;
  this.project_assets = __dirname + "/app/gulp/";
  this.public_dir = "public"
  this.short_commit_hash = process.env.COMMIT_SHORT_SHA ? process.env.COMMIT_SHORT_SHA : "latest";

  this.settings = {
    "name": "kisphp demo",
    "root_path": this.root_path,
    "project_assets": this.project_assets,

    "js": {
      "external": {
        "sources": [
          'node_modules/kisphp-format-string/src/format-string.js',
          this.project_assets + 'assets/js/external.js'
        ],
        "output_filename": "external-" + this.short_commit_hash + ".js",
        "output_dirname": this.public_dir + "/js/",
      },
      "project": {
        "sources": [
          this.project_assets + '/assets/js/app.js',
        ],
        "output_filename": "app-" + this.short_commit_hash + ".js",
        "output_dirname": this.public_dir + "/js/",
      }
    },
    "css": {
      "external": {
        "sources": [
          'node_modules/bootstrap/dist/css/bootstrap.min.css'
        ],
        "output_filename": "external-" + this.short_commit_hash + ".css",
        "output_dirname": this.public_dir + "/css/",
      },
      "project": {
        "sources": [
          this.project_assets + '/assets/css/project.css'
        ],
        "output_filename": "app-" + this.short_commit_hash + ".css",
        "output_dirname": this.public_dir + "/css/",
      },
      "stylus": {
        "sources": [
          this.project_assets + '/assets/css/main.styl'
        ],
        "output_filename": "stylus-" + this.short_commit_hash + ".css",
        "output_dirname": this.public_dir + "/css/",
      },
      "scss": {
        "sources": [
          this.project_assets + '/assets/css/main.scss'
        ],
        "output_filename": "scss-" + this.short_commit_hash + ".css",
        "output_dirname": this.public_dir + "/css/",
      }
    },
    "files": {
      "images": {
        "sources": [
          this.project_assets + '/assets/images/**/*.*',
        ],
        "output_dirname": this.public_dir + "/images"
      }
    }
  };

  return this.settings;
};

module.exports = settings();
