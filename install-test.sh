#!/bin/bash

echo "Create package.json file"
cat << EOF > package.json
{
  "scripts": {
    "build": "gulp --gulpfile app/gulp/gulpfile.js --cwd ."
  }
}
EOF

echo "Install dependencies"
npm install kisphp-assets jquery bootstrap npm install @fortawesome/fontawesome-free

echo "Create necessary directories"
mkdir -p app/gulp/assets/{css,images,js}
mkdir -p app/gulp/assets/js/modules
mkdir public
touch app/gulp/assets/css/main.css

echo "Copy required files"
cp ../gulpfile.js app/gulp/gulpfile.js
cp ../gulp-config.dist.js ./gulp-config.js

cat << EOF > app/gulp/assets/js/modules/demo.js
module.exports = {
    init: function() {
        console.log('file loaded');
    }
}
EOF

cat << EOF > app/gulp/assets/js/app.js
\$(document).ready(function(){
    require('./modules/demo').init();
    // add here more files that do one thing (Single Responsibility Principle)
});
EOF

cat << EOF > public/index.html
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kisphp Assets demo</title>
    <link rel="stylesheet" href="/css/external-latest.css" />
    <link rel="stylesheet" href="/css/app-latest.css" />
</head>
<body>

<h1>Kisphp Assets - demo</h1>

<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-light">Light</button>
<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-link">Link</button>

<script src="/js/external-latest.js"></script>
<script src="/js/app-latest.js"></script>
</body>
</html>
EOF

npm run build

cat << EOF
Thank you for installing Kisphp Assets

This script have installed all files for you and created
a demo html page in public directory.

If you have php installed, run the following command to start the server:
"""
php -S localhost:8080 -t public
"""

If you want to use http-server from nodejs, run the following commands:

"""
npm install --save-dev http-server
./node_modules/.bin/http-server
"""

In both cases, open the following url in your browser:

http://127.0.0.1:8080
EOF
