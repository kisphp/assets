const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const concat = require('gulp-concat');

const strip = require('gulp-strip-comments');

let config_content = {};

function javascripts(cb) {
    src(config_content.sources)
        .pipe(plumber())
        .pipe(strip())
        .pipe(concat(config_content.output_filename))
        .pipe(dest(config_content.output_dirname))
    ;

    cb();
}

module.exports = function(config){
    config_content = config;
    return { javascripts };
};
