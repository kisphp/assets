const { src, dest } = require('gulp');

let config_content = {};

function copy_files(cb){
  for (const [key, value] of Object.entries(config_content)) {
    let sources = value.sources;

    if (!Array.isArray(sources) || !sources.length) {
      continue;
    }

    src(sources)
        .pipe(dest(value.output_dirname), (err) => {
          if (err) {
            console.error(`Error copying files for key: ${key}`, err);
            throw Error(`Error copying files for key: ${key}`);
          }
        })
    ;
  }

  cb();
}

module.exports = function(config){
  config_content = config;

  return { copy_files };
};
