const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const concat = require('gulp-concat');

const strip = require('gulp-strip-comments');
const brsfy = require('browserify');
const through2 = require('through2');
const minify = require('gulp-minify');

let config_content = {};

/**
 * source: https://stackoverflow.com/questions/28441000/chain-gulp-glob-to-browserify-transform/28441837#28441837
 *
 * @returns {*}
 */
function browserified(){
    return through2.obj(function(file, enc, next){
        brsfy(file.path, {
            debug: true
        })
            .bundle(function(err, res){
                if (err) {
                    return next(err);
                }

                file.contents = res;

                next(null, file)
            })
        ;
    });
}

function browserify(cb){
    src(config_content.sources)
        .pipe(plumber())
        .pipe(browserified())
        .pipe(strip())
        .pipe(minify({
            noSource: true
        }))
        .pipe(concat(config_content.output_filename))
        .pipe(dest(config_content.output_dirname))
    ;

    cb();
}

module.exports = function(config){
    config_content = config;
    return { browserify };
};
