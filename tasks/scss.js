const { src, dest } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const plumber = require('gulp-plumber');
const concat = require('gulp-concat');

let config_content = {};

function scss(cb){
  src(config_content.sources)
      .pipe(plumber())
      .pipe(sass())
      .pipe(concat(config_content.output_filename))
      .pipe(dest(config_content.output_dirname))
  ;

  cb()
}

module.exports = function(config){
  config_content = config;
  return { scss };
};
