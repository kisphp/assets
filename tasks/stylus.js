const { src, dest } = require('gulp');

let config_content = {};

const plumber = require('gulp-plumber');
const concat = require('gulp-concat');
const sty = require('gulp-stylus');

function stylus(cb){
  src(config_content.sources)
      .pipe(plumber())
      .pipe(sty({
        compress: true
      }))
      .pipe(concat(config_content.output_filename))
      .pipe(dest(config_content.output_dirname))
  ;

  cb()
}

module.exports = function(config){
  config_content = config;
  return { stylus };
};
