# Kisphp assets management with GULP

This tool will allow you to compile javascripts, css and copy assets file to your public directory in a very easy way.

## Requirements

> You need to have [NodeJS 16](https://nodejs.org/) installed and [GulpJS](https://gulpjs.com/docs/en/getting-started/quick-start#install-the-gulp-command-line-utility)
> ```bash
> npm install --global gulp
> ```

## Install

Follow the next steps to create the necessary file for the tool integration

> If you want to have a quick demo or a quick setup and you have an empty directory,
> run the following command to install and add the files automatically:
>
> ```bash
> curl -s https://gitlab.com/kisphp/assets/raw/main/install.sh | bash -
> ```

### `package.json` file

Create the `package.json` script in the root directory of the project with the following content:

```json
{
  "scripts": {
    "build": "gulp --gulpfile app/gulp/gulpfile.js --cwd ."
  }
}
```

For the moment, the `package.json` file will only have the gulp execution script defined. We also need some dependencies defined in this file, but we'll add them with the following command.

Run the following command to install the dependencies and save them into `package.json` file:

```bash
npm install --save kisphp-assets jquery bootstrap
```

After the command finishes, you'll see that inside the `package.json` file will appear a list of dependencies that we'll need for our project.

#### Create gulp assets directory:

```bash
mkdir -p app/gulp/assets/{css,images,js}
mkdir -p app/gulp/assets/js/modules
touch app/gulp/assets/css/main.css
```

#### Copy `gulpfile.js` in `app/gulp/` directory

```bash
cp node_modules/kisphp-assets/gulpfile.js app/gulp/gulpfile.js
```

#### Copy `gulp-config.dist.js` in your application root directory and remove `dist` from its name

```bash
cp node_modules/kisphp-assets/gulp-config.dist.js ./gulp-config.js
```

#### Create the `app/gulp/assets/js/modules/demo.js` file with the content:

```js
cat << EOF > app/gulp/assets/js/modules/demo.js
module.exports = {
    init: function() {
        console.log('file loaded');
    }
}
EOF
```
In the `app/gulp/assets/js/modules/` directory you'll store all javascript modules for your website.
A best practice is to minimize the dependencies between those files as much as possible. 
Every file will export an object with the `init()` function that will be called in the following file.


#### Create the `app/gulp/assets/js/app.js` with the following content:

```js
cat << EOF > app/gulp/assets/js/app.js
$(document).ready(function(){
    require('./modules/demo').init();
    // add here more files that do one thing (Single Responsibility Principle)
});
EOF
```

This is file where you load all your javascript modules.

#### Run gulp script to compile all assets file

```bash
npm run build
```

When the command will finish you'll see new directories and files inside the public directory.

```bash
public
|-- css
|   |-- app.css
|   `-- external.css
`-- js
    |-- app.js
    `-- external.js
```

#### Load generated assets files into your html code

To load the generated files into your html template, you need to add the css files inside the `<head>...</head>` tags:

```html
<link rel="stylesheet" href="/css/external.css" />
<link rel="stylesheet" href="/css/app.css" />
```

And then add the javascript files right before the `</body>` closing tag:

```html
<script src="/js/external.js"></script>
<script src="/js/app.js"></script>
```

## Examples of configuration

Here is a fully functional example of how to use this library.

Create the following files in a new directory

#### `app/gulp/assets/css/main.css`
```stylus
.alfa {
  background: #369;
  color: #fff;
}
.beta {
  background: #6f42c1;
  color: #fff;
}
.gama {
  background #a52834;
  color: #fff;
}
```


#### Load assets file in your `public/index.html` or `public/index.php` file

```html
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example</title>
    <link rel="stylesheet" href="/css/external.css" />
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col alfa">
            1 of 3
        </div>
        <div class="col-6 beta">
            2 of 3 (wider)
        </div>
        <div class="col gama">
            3 of 3
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 clickme">Click me</div>
        <div class="col"><span id="counter">0</span></div>
    </div>
</div>

<script src="/js/external.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
```

Create the counter js file in `app/gulp/assets/js/modules/counter.js` with the following content:

```javascript
module.exports = {
    init: function (){
        $('.clickme').on('click', function (ev){
            ev.preventDefault();

            let counter = $('#counter');
            let counterValue = counter.html();

            counter.html(parseInt(counterValue) + 1);
        });
    }
};
```

Then, you need to register the new javascript module to the application

Open the file `app/gulp/assets/js/app.js` and add your new module:

```js
$(document).ready(function(){
    require('./modules/demo').init();
    require('./modules/counter').init();
});
```

Once you have all these files run the following commands to compile the dependencies

```bash
npm install

npm run build
```

If you have php installed, run the following command:

```bash
php -S localhost:8000 -t public
```

And open the url [http://localhost:8000/](http://localhost:8000/) in your browser

# SCSS support

The tool has a script to compile scss files, but the dependencies are not installed. If you use scss, run the following command to install the necessary dependencies:

```bash
npm i -S gulp-sass
npm i -S sass
```
