# Publish new version

- Change the version in `package.json` file
- Create tag on git repository
- Run `npm publish` 
